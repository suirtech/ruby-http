require "net/http"
require "uri"
require 'openssl'

class Requester


  # http get method dont need to touch these
  def self.do_get(url)
    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    return response.body
  end


  def self.do_post_json(url, body)
    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)

    request = Net::HTTP::Post.new(uri.request_uri)
    request.set_body_internal(body)

    response = http.request(request)

  end


  def self.do_post_form(url, username, password)
    form_str = "username=#{username}&password=#{password}&rememberme=true&policyAgreed="
    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(uri.request_uri)
    request.set_body_internal(form_str)

    response = http.request(request)
    return response
  end

end




