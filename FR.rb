require './requester.rb'
require 'json'
require 'test/unit/assertions'

extend Test::Unit::Assertions


class FR

def self.search(origin, dest)
    url = "https://desktopapps.ryanair.com/v3/en-ie/availability?" \
          "ADT=1&" \
          "CHD=0&" \
          "DateOut=2017-10-25&" \
          "Destination=#{dest}&" \
          "FlexDaysOut=4&" \
          "INF=0&" \
          "IncludeConnectingFlights=true&" \
          "Origin=#{origin}&" \
          "RoundTrip=false&" \
          "TEEN=0&" \
          "ToUs=AGREED&" \
          "exists=false&" \
          "promoCode="
    return Requester.do_get url
  end

  def self.login(username, password)
    url = "https://sit-api.ryanair.com/userprofile/rest/api/v1/login"
    Requester.do_post_form(url, username, password)
  end

  def self.search_rooms(place='Dublin')
    url = "https://aggregator-rooms.ryanair.com/ha/v2/static-content?checkinDate=2017-10-22&checkoutDate=2017-11-16&roomAllocations=2&page=0&place=#{place}&currencyCode=EUR&size=30&market=en-ie"
    return Requester.do_get(url)
  end


  def self.get_total_from_search(place)
    return JSON.parse(FR.search_rooms(place))['total']
  end
end


#call searches
# puts FR.search "DUB", "WRO"
#
# puts FR.search "DUB", "STN"


# call login and print body and code of response
# login_response = FR.login "shane.connolly.ios%40gmail.com", "Testing123"
#
# puts "login rep code : #{login_response.code}"
# puts "rep body :: #{login_response.body}"

t = FR.get_total_from_search ('Madrid')
assert_equal( 902, t, "Wrong total returned" )


